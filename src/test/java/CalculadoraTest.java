import mundo.Calculadora;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    @Test
    public void testEspaciados() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar("    1    +    1    ");

        Assertions.assertEquals("2.0", resultado);

    }

    @Test
    public void testDouble() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 2.3 + 1.6 ");

        Assertions.assertEquals("3.9", resultado);

    }

    @Test
    public void testOtroOperador() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 3 ^ 3 ");

        Assertions.assertEquals("Operador no soportado.", resultado);

    }

    @Test
    public void testSumaPositiva() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 5 + 12 ");

        Assertions.assertEquals("17.0", resultado);

    }

    @Test
    public void testSumaNegativa() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 2 + -5 ");

        Assertions.assertEquals("-3.0", resultado);

    }

    @Test
    public void testRestaPositiva() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 6 - 9 ");

        Assertions.assertEquals("-3.0", resultado);

    }

    @Test
    public void testRestaNegativa() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 3 - -3 ");

        Assertions.assertEquals("6.0", resultado);

    }

    @Test
    public void testRestaCero() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 0 - 3 ");

        Assertions.assertEquals("-3.0", resultado);

    }

    @Test
    public void testMultiplicacionPositiva() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 4 * 3 ");

        Assertions.assertEquals("12.0", resultado);

    }

    @Test
    public void testMultiplicacionNegativa() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 5 * -5 ");

        Assertions.assertEquals("-25.0", resultado);

    }

    @Test
    public void testMultiplicacionCero() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 5 * 0 ");

        Assertions.assertEquals("0.0", resultado);

    }

    @Test
    public void testMultiplicacionDobleCero() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 0 * 0 ");

        Assertions.assertEquals("0.0", resultado);

    }

    @Test
    public void testDivisionPositiva() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 9 / 2 ");

        Assertions.assertEquals("4.5", resultado);

    }

    @Test
    public void testDivisionNegativa() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 4 / -2 ");

        Assertions.assertEquals("-2.0", resultado);

    }

    @Test
    public void testDivisionCeroNumerador() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 0 / 4 ");

        Assertions.assertEquals("0.0", resultado);

    }

    @Test
    public void testDivisionCeroDenominador() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 9 / 0 ");

        Assertions.assertEquals("Infinity", resultado);

    }

    @Test
    public void testDivisionDobleCero() {

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.operar(" 0 / 0 ");

        Assertions.assertEquals("NaN", resultado);

    }

}
