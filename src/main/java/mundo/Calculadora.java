package mundo;

public class Calculadora {

    public String operar(String pTexto){

        String resultado;
        String textoSinEspacios = pTexto.replaceAll("\\s+", "");
        char operador = textoSinEspacios.replaceAll("[0-9,.]", "").charAt(0);
        Double numeroUno = Double.parseDouble(textoSinEspacios.substring(0, textoSinEspacios.indexOf(operador)));
        Double numeroDos = Double.parseDouble(textoSinEspacios.substring(textoSinEspacios.indexOf(operador) + 1));

        switch (operador){

            case '+':
                resultado = Double.toString(numeroUno + numeroDos);
                break;
            case '-':
                resultado = Double.toString(numeroUno - numeroDos);
                break;
            case '*':
                resultado = Double.toString(numeroUno * numeroDos);
                break;
            case '/':
                resultado = Double.toString(numeroUno / numeroDos);
                break;
            default:
                resultado = "Operador no soportado.";
                break;

        }

        return resultado;

    }

}
